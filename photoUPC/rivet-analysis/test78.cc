// Main program to generate "MB" gm+p events and run use a rivet analysis
// for data comparison

// Author: Ilkka Helenius, April 2021.

#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/Pythia8Rivet.h"

using namespace Pythia8;

// The main program.

int main() {

  // Generator.
  Pythia pythia;

  // Decrease the output.
  pythia.readString("Init:showChangedSettings = off");
  pythia.readString("Init:showChangedParticleData = off");
  pythia.readString("Next:numberCount = 10000");
  pythia.readString("Next:numberShowInfo = 0");
  pythia.readString("Next:numberShowProcess = 0");
  pythia.readString("Next:numberShowEvent = 10");

  // Beam parameters for gm+p collisions
  pythia.readString("Beams:frameType = 2");
  pythia.readString("Beams:eA = 820.");
  pythia.readString("Beams:eB = 12.195");
  pythia.readString("Beams:idA = 2212");
  pythia.readString("Beams:idB = 22");

  // Turn on relevant processes
  pythia.readString("PhotonParton:all = on");
  pythia.readString("SoftQCD:nonDiffractive = on");

  // Vary the screening parameter.
  pythia.readString("MultipartonInteractions:pT0Ref = 3.00");

  // Initialize the generator.
  pythia.init();

  // Number of events (~1 min).
  int nEvents = 100000;

  // Initialize the communication with the Rivet program.
  Pythia8Rivet rivet(pythia, "Pythia_pT0ref_300.yoda");
  rivet.addAnalysis("H1_1999_I477556");

  // Begin event loop. Skip if fails.
  for (int iEvent = 0; iEvent < nEvents; ++iEvent) {

    // Generate next event.
    if (!pythia.next()) continue;

    // Send the event to Rivet.
    rivet();

  } // End of event loop.

  // Show statistics.
  pythia.stat();

  // Done.
  rivet.done();
  return 0;
}
