import matplotlib.pyplot as plt
import numpy as np

plt.rc('text', usetex=True)
plt.rc('font', family='serif',size=16)

x1,y1 = np.loadtxt("nCH_epHERA_direct_pT10.dat", unpack = True)
x2,y2 = np.loadtxt("nCH_epHERA_resolved_pT10.dat", unpack = True)

plt.step(x1, y1, color='C0', where='mid', label='Direct')
plt.step(x2, y2, color='C1', where='mid', label='Resolved')

plt.yscale('log', nonpositive='clip')
#plt.ylim(ymin=1.e0, ymax=1e4)
plt.xlim(xmin=0.e0, xmax=2.e2)

plt.xlabel(r"$n_{\mathrm{ch}}$")
plt.ylabel(r"Number of Events")

plt.legend(frameon=False, numpoints=1, loc='upper right')
plt.savefig("mult_HERA.pdf", bbox_inches='tight')

