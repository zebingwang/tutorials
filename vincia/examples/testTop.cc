// Modified version of test273.cc for Vincia tutorial.
// Compare Pythia (with various options) and Vincia on simplified top mass
// reconstruction. 

// test273.cc is a part of the PYTHIA event generator.
// Copyright (C) 2020 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.


// Important: the top mass shift analysis encoded here is very primitive,
// does not perform well at all, and should not be taken seriously.
// The important part is that you see how the different scenarios
// should be set up to operate as intended.

#include "Pythia8/Pythia.h"

// Include a UserHook for SimpleShower that corrects emission in top
// decay for dipole from gluon to W, to instead be from gluon to top.
#include "topCoherenceHook.h"

using namespace Pythia8;

//==========================================================================

int main() {

  // Number of events to generate.
  // Warning: much statistics is needed for significant results,
  // so this is just an appetizer. Anyway, the reconstruction is
  // pretty lousy, so not useful for real studies.
  int nEvent = 5000;
  
  // Set up anti-kT jet finder with R = 0.5 and pTmin = 20 GeV.
  double Rjet = 0.5;
  double pTjetMin = 20.;
  SlowJet sJet( -1, Rjet, pTjetMin);
  
  // Generator at LHC. 
  Pythia pythia;
  Event& event = pythia.event;
  pythia.readString("Beams:eCM = 13000.");
  
  // Reduce printout.
  pythia.readString("Init:showChangedParticleData = off");
  pythia.readString("Next:numberShowInfo = 0");
  pythia.readString("Next:numberShowProcess = 0");
  pythia.readString("Next:numberShowEvent = 1");
  pythia.readString("Next:numberCount = 500");
    
  // Switch off MPI, hadronisation, and W decay (for super-simplicity!)
  pythia.readString("PartonLevel:MPI = off");
  pythia.readString("HadronLevel:all = off");
  pythia.readString("24:mayDecay = off");
    
  // Setup for top production.
  pythia.readString("Top:qqbar2ttbar = on");
  pythia.readString("Top:gg2ttbar = on");

  // -------- THIS IS WHERE YOU EDIT ----------------------------------------

  // OPTIONS FOR CURRENT RUN. 
  // Set up the run options you want and file name for output histograms.
  //pythia.readString("TimeShower:recoilToColoured = off");
  bool doTopCoherence = false;
  
  // DO NOT FORGET TO CHANGE NAME OF OUTPUT FILE WHEN CHANGING OPTIONS.
  string saveFile = "default.dat";
  //string saveFile = "topHook.dat";
  //string saveFile = "vincia.dat";
  //string saveFile = "vincia-nonIRD.dat";

  // -------------------------------------------------------------------------
  
  // Set up to do eikonal correction if requested.
  // Hook is only intended for SimpleShowers with recoilToColoured = off.
  // (For added safety, it checks internally if those options have the
  // expected values, and otherwise does nothing.)
  UserHooksPtr topCoherenceHook = make_shared<TopCoherenceHook>();
  if (doTopCoherence) pythia.setUserHooksPtr( topCoherenceHook );
    
  // Do the initialisation.
  pythia.init();
    
  // Get value of target top mass (to compare to).
  double mT = pythia.particleData.m0(6);

  // Histograms for current scenario.
  Hist nJetH(  "jet multiplicity",              10, -0.5, 9.5);
  Hist mTerrH( "reconstructed t mass error",    25, -15.,  10.0);

  // Begin event loop. Generate event. Skip if error.
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {
    if (!pythia.next()) continue;

    // Find W bosons.
    vector<int> iW;
    for (int i = 0; i < event.size(); ++i) {
      if (event[i].idAbs() != 24) continue;
      // Set status < 0 so not included in jet clustering below.
      event[i].statusNeg();
      iW.push_back(i);
    }
    
    // Find number of jets. At least two to keep going.
    sJet.analyze(event);
    int nJet = sJet.sizeJet();
    nJetH.fill( nJet, pythia.info.weight());
    if (nJet < 2) continue;

    // Find best jet-W mass pair, closest to reference mT.
    double bestDiff = 1e10;      
    double mWJrec = 0.;
    for (int i = 0; i <= 1; ++i) {        
      for (int j=0; j<nJet; ++j) {
        double mWJ = (event[iW[i]].p() + sJet.p(j)).mCalc();
        if (abs(mWJ - mT) < bestDiff) {
          mWJrec  = mWJ;
          bestDiff = abs(mWJ - mT);
        }
      }
    }
    // Fill histogram with top mass difference wrt reference value.
    mTerrH.fill( mWJrec - mT , pythia.info.weight() );    
  }

  // Normalise histograms.
  nJetH  *= 1. / pythia.info.weightSum();
  mTerrH *= 1. / pythia.info.weightSum();
  // Both have a bin width of 1, so no bin-width normalisation.
  // Normalise mTerrH to "per Top" (2 tops per event)
  mTerrH /= 2;
  
  pythia.stat();
  cout <<  nJetH << mTerrH;
  stringstream ss;
  ss<<"mTerr-"<<saveFile;
  mTerrH.table(ss.str());
  ss.str("");
  ss<<"nJet-"<<saveFile;
  nJetH.table(ss.str());

  // Done.
  return 0;
}
