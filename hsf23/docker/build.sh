#!/bin/bash

# Grab the Pythia source and build the full Python interface.
if [ ! -f pythia8309.tgz ]; then
    wget https://pythia.org/download/pythia83/pythia8309.tgz
    tar -xzvf pythia8309.tgz && rm pythia8309.tgz 
    cd pythia8309/plugins/python
    ./generate --full
    cd - && tar -czvf pythia8309.tgz pythia8309
    rm -rf pythia8309
fi

# Build the Docker container.
docker build --network=host -t pythia8/tutorials:hsf23 .
